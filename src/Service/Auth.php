<?php

namespace GranitSDK\Service;

use GranitSDK\Di;
use GranitSDK\Service\Auth\User;

class Auth
{
	const SESSION_KEY = 'auth.user';

	protected $user;
	protected $userClass;

	public function __construct()
	{
		if ($this->hasUserInSession()) {
			$this->user = $this->getUserFromSession();
		}
		else {
			$this->user = $this->getGuestUser();
		}
	}

	protected function getGuestUser()
	{
		return new User();
	}

	protected function saveUserToSession()
	{
		Di::get()->getSession()->set(self::SESSION_KEY, serialize($this->user));
	}

	protected function hasUserInSession()
	{
		$user = $this->getUserFromSession();
		if (empty($user)) {
			return false;
		}
		return ($user instanceof User);
	}

	protected function getUserFromSession()
	{
		return @unserialize(Di::get()->getSession()->get(self::SESSION_KEY));
	}

	public function getUser()
	{
		return $this->user;
	}

	public function clearUser()
	{
		$this->user = $this->getGuestUser();
		$this->saveUserToSession();
	}

	public function setUser(User $user)
	{
		$this->user = $user;
		$this->saveUserToSession();
	}
}
<?php

namespace GranitSDK\Service\Auth;


class User implements \JsonSerializable
{
	protected $id;
	protected $class;
	protected $email;
	protected $name;
	protected $roles = [];

	public function getId()
	{
		return $this->id;
	}

	public function setId($id): self
	{
		$this->id = $id;
		return $this;
	}

	public function getEmail()
	{
		return $this->email;
	}

	public function setEmail($email): self
	{
		$this->email = $email;
		return $this;
	}

	public function getName()
	{
		return $this->name;
	}

	public function setName($name): self
	{
		$this->name = $name;
		return $this;
	}

	public function getClass()
	{
		return $this->class;
	}

	public function setClass($class): self
	{
		$this->class = $class;
		return $this;
	}

	public function getRoles(): array
	{
		return $this->roles;
	}

	public function setRoles(array $roles): self
	{
		$this->roles = $roles;
		return $this;
	}

	public function isAdmin()
	{
		return in_array('ROLE_ADMIN', $this->roles);
	}

	public function isCustomer()
	{
		return in_array('ROLE_CUSTOMER', $this->roles);
	}

	public function isPartner()
	{
		return in_array('ROLE_PARTNER', $this->roles);
	}

	public function isGuest()
	{
		return empty($this->roles);
	}

	public function jsonSerialize()
	{
		return [
			'id' => $this->getId(),
			'name' => $this->getName(),
			'email' => $this->getEmail(),
		];
	}

	public function getIdentify()
	{
		return $this->getId() . ':' . $this->getClass();
	}
}